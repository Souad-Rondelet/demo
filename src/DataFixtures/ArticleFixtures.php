<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $article = new Article();
            $article->setTitle("Titre de l'article n°$i")
                    ->setContent("<p>Contenu de l'article rgf ig i iugil g glkg 
                    ifgu g  gilh uZG<JB QGLIG Isrohgo o o hohgekjq omqhgkgh ohghg kjgk ghlk hglwhgl kwjghlwn°$i</p>")
                    ->setImage("/image/couples.jpg")
                    ->setCreatedAt(new \DateTime());

            $manager->persist($article);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
